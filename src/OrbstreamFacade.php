<?php namespace Orbscope\Orbstream\Laravel;

use Illuminate\Support\Facades\Facade;

class OrbstreamFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'orbstream'; }
}
