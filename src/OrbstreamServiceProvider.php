<?php

namespace Orbscope\Orbstream\Laravel;

use Illuminate\Support\ServiceProvider;

class OrbstreamServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('orbstream', function(){
            return new \Orbscope\Orbstream\Client(
                env('ORBSTREAM_BASE_URL', 'https://orbstream.orbscope.com/'),
                env('ORBSTREAM_APP_ID', ''),
                env('ORBSTREAM_APP_SECRET', '')
            );
        });
    }
}
