Orbstream Laravel SDK
=======================
> client SDK for Orbscope/Orbstream based on `orbscope/orbstream` package.

Usage
======================
- `composer require orbscope/orbstream-laravel`
- register the service provider `Orbscope\Orbstream\Laravel\OrbstreamServiceProvider::class` in `config/app.php` in `providers`.
- register the facade `Orbscope\Orbstream\Laravel\OrbstreamFacade`

```php
<?php

	echo Orbstream::uniqid();

	Orbstream::publish("mychannel");

	dd(Orbstream::clients("mychannel"));

```
